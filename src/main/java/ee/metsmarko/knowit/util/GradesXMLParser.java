package ee.metsmarko.knowit.util;

import ee.metsmarko.knowit.common.Student;
import ee.metsmarko.knowit.common.Subject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that deals with parsing of the xml file.
 */
public class GradesXMLParser {

    private final String FILE_NAME = "grades.xml";

    private final String STUDENT = "student";
    private final String SUBJECT = "subject";
    private final String NAME = "name";
    private final String GRADE = "grade";

    private final String CREDITS = "credits";
    File file;


    public GradesXMLParser() {
        URL url = this.getClass().getClassLoader().getResource(FILE_NAME);
        file = new File(url.getFile());
    }

    /**
     * Parses the XML document and returns an list of student.
     *
     * @return List of students.
     */
    public List<Student> parseXml() {
        Document doc = getDocument();
        List<Student> studentList = new ArrayList<Student>();

        NodeList studentNodeList = doc.getElementsByTagName(STUDENT);
        for (int i = 0; i < studentNodeList.getLength(); i++) {
            Node nNode = studentNodeList.item(i);
            Element eElement = (Element) nNode;
            Student s = getStudent(eElement);
            studentList.add(s);
        }
        return studentList;
    }

    /**
     * Constructs a student object.
     * @param eElement Student element.
     * @return student.
     */
    private Student getStudent(Element eElement) {
        NodeList subjects = eElement.getElementsByTagName(SUBJECT);
        String studentName = eElement.getAttribute(NAME);
        List<Subject> subjectList = getSubjects(eElement, subjects);
        return new Student(studentName, subjectList);
    }

    /**
     * Method returns all subjects that are associated with given user.
     * @param eElement Student element.
     * @param subjects NodeList of subjects.
     * @return List of subjects.
     */
    private List<Subject> getSubjects(Element eElement, NodeList subjects) {
        List<Subject> subjectList = new ArrayList<Subject>();
        for (int j = 0; j < subjects.getLength(); j++) {
            int grade = Integer.parseInt(eElement.getElementsByTagName(GRADE).item(j).getTextContent());
            int credits = Integer.parseInt(eElement.getElementsByTagName(CREDITS).item(j).getTextContent());
            Subject s = new Subject(grade, credits);
            subjectList.add(s);
        }
        return subjectList;
    }

    /**
     * Constructs document.
     * @return document.
     */
    private Document getDocument() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            doc = dBuilder.parse(file);
        } catch (SAXException e) {
        } catch (IOException e) {
        }

        doc.getDocumentElement().normalize();
        return doc;
    }

}
