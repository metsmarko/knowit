package ee.metsmarko.knowit.common;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Class represents a student.
 */
@JsonIgnoreProperties({"subjects"})
public class Student implements Comparable<Student> {

    private String name;
    private double weightedAverage;
    private List<Subject> subjects;

    /**
     * Constructs a new student.
     *
     * @param name     Student's name.
     * @param subjects List of all the subjects that this student has taken.
     */
    public Student(String name, List<Subject> subjects) {
        this.name = name;
        this.subjects = subjects;
    }

    public double getWeightedAverage() {
        return weightedAverage;
    }

    public String getName() {
        return name;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    /**
     * Calculates weighted average of the subjects.
     */
    public void calculateWeightedAverage() {
        int totalCredits = 0;
        int productSum = 0;
        for (Subject s : subjects) {
            totalCredits += s.getCredits();
            productSum += s.getCredits() * s.getGrade();
        }
        if (totalCredits == 0) {
            this.weightedAverage = 0;
        } else {
            this.weightedAverage = productSum / (double) totalCredits;
        }
    }

    @Override
    public int compareTo(Student o) {
        if (this.getWeightedAverage() < o.getWeightedAverage()) {
            return -1;
        } else if (this.getWeightedAverage() > o.getWeightedAverage()) {
            return 1;
        } else {
            return this.getName().compareTo(o.getName())*-1;
        }
    }
}
