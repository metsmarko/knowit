package ee.metsmarko.knowit.common;

/**
 * Class represents a subject. Holds student's grade for this subject and credits.
 */
public class Subject {

    private int grade, credits;


    public Subject(int grade, int credits) {
        this.grade = grade;
        this.credits = credits;
    }

    public int getGrade() {
        return grade;
    }

    public int getCredits() {
        return credits;
    }

}
