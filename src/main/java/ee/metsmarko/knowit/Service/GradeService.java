package ee.metsmarko.knowit.Service;

import ee.metsmarko.knowit.common.Student;
import ee.metsmarko.knowit.util.GradesXMLParser;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.sort;

/**
 * Created by Marko Mets on 17.11.2014.
 */
public class GradeService {


    /**
     * Returns list of students that is sorted by their weighted average grade.
     * @return sorted list of students.
     */
    public List<Student> getStudentsSortedByWeightedGrade(){
        List<Student> students = parseXML();
        calculateWeightedAverage(students);
        sort(students);
        Collections.reverse(students);
        return students;
    }

    private void calculateWeightedAverage(List<Student> students) {
        for(Student s : students){
            s.calculateWeightedAverage();
        }
    }

    private List<Student> parseXML() {
        return new GradesXMLParser().parseXml();
    }

}
