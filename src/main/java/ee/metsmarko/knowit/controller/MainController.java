package ee.metsmarko.knowit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/knowit")
public class MainController {
	@RequestMapping(method = RequestMethod.GET)
	public String getIndexPage() {
		return "/static/index.html";
	}
}