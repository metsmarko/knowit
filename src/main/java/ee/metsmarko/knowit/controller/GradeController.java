package ee.metsmarko.knowit.controller;

import ee.metsmarko.knowit.Service.GradeService;
import ee.metsmarko.knowit.common.Student;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marko Mets on 17.11.2014.
 */
@Controller
@RequestMapping("/grade")
public class GradeController {

    @RequestMapping(method = RequestMethod.GET )
    public @ResponseBody
    List<Student> getStudentsByWeightedGrade(){
        return new GradeService().getStudentsSortedByWeightedGrade();
    }

}
