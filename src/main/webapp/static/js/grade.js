/**
 * Created by Marko Mets on 18.11.2014.
 */

$(document).ready(function () {
    retrieveStudentsGrades();
});

function retrieveStudentsGrades() {
    $.ajax({
        url: "/grade",
        success: function (answer) {
            printResults(answer);
        },
        error: function () {
            console.log("no grades");
        }
    });
}
function printResults(answer) {
    var gradesTable = "<table class='table table-bordered'>";
    gradesTable += "<th>Name</th>"
    gradesTable += "<th>Grade</th>"
    $.each(answer, function (i, obj) {
        var name = obj.name;
        var grade = obj.weightedAverage;
        gradesTable = createTable(gradesTable, name, grade);
    });
    gradesTable += "</table>";
    $("#grades").empty();
    $("#grades").append(gradesTable);
}
function createTable(gradesTable, name, grade) {
    gradesTable += "<tr>";
    gradesTable += "<td>" + name + "</td>";
    gradesTable += "<td>" + grade + "</td>";
    gradesTable += "</tr>";
    return gradesTable;
}
