package ee.metsmarko.knowit;

import ee.metsmarko.knowit.common.Student;
import ee.metsmarko.knowit.common.Subject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Marko Mets on 17.11.2014.
 */
public class StudentTest {

    @Test
    public void weightedAverageTest(){
        List<Subject> subjects = new ArrayList<Subject>();
        subjects.add(new Subject(4,6));
        subjects.add(new Subject(5,6));
        subjects.add(new Subject(2,3));
        Student s = new Student("Test Student", subjects);
        s.calculateWeightedAverage();
        assertEquals(new Double(4), (Double) s.getWeightedAverage());
    }

    @Test
    public void weightedAverageZeroTest(){
        List<Subject> subjects = new ArrayList<Subject>();
        subjects.add(new Subject(0,0));
        Student s = new Student("Test Student", subjects);
        s.calculateWeightedAverage();
        assertEquals(new Double(0), (Double) s.getWeightedAverage());
    }

    @Test
    public void weightedAverageNoSubjectsTest(){
        List<Subject> subjects = new ArrayList<Subject>();
        Student s = new Student("Test Student", subjects);
        s.calculateWeightedAverage();
        assertEquals(new Double(0), (Double) s.getWeightedAverage());
    }

}
